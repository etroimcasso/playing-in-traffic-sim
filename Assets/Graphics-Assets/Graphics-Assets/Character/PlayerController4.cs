﻿using UnityEngine;
using System.Collections;

public class PlayerControllerRyan : MonoBehaviour 
{
	public Animator anim;

	private float inputH;

	private float inputV;


	private Rigidbody rb;



	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		rb = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		inputH = Input.GetAxis ("Vertical");

		inputV = Input.GetAxis ("Horizontal");

		anim.SetFloat ("inputH", inputH);

		anim.SetFloat ("inputV", inputV);

		float moveX = inputH * 20f * Time.deltaTime;
		float moveZ = inputV * 50f * Time.deltaTime;

		rb.velocity = new Vector3 (moveX, 0f, moveZ);


	}

}