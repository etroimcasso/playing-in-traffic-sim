﻿using UnityEngine;
using System.Collections;

//DayNightCycleController will rotate the sun object to create a simple day/night cycle.
//--Dims the sun as well
//Also Illuminates TrafficLight Objects along the road

//NEEDS:
//•isGameActive boolean and setter, as per all GameController processes
//•Day and Night lengths in the form of seconds on float
//•Sun becomes moon at night
//•Do this by changing the color of the sun to a moon color, starting back at the beginning of the day cycle, but cycle in the opposite direction and with the night time instead of day
public class DayNightCycleController : MonoBehaviour {
	public GameController gameController;
	public bool dayNightCycleEnabled = true;
	private bool isGameActive = false;

	//Use a struct instead of class because struct is faster
	[System.Serializable]
	public struct LightingObjects {
		public GameObject sun;
		public Light[] lights;
	}

	[System.Serializable]
	public struct CycleSettings {
		public uint dayLength; //In Seconds
		public uint nightLength;
		public float sunYRotation;
		public Day daySettings;
		public Night nightSettings;


		public struct Day {
			public uint length;
			public Color sunColor;
			public float beginY;
		}

		public struct Night {
			public uint length;
			public Color moonColor;
			public Color streetLightColor;
			public Color headlightColor;
			public float beginY;
		}
	}


	public enum CycleMode
	{
		Day = 0,
		Night = 1
	}
	private CycleMode dayOrNight = CycleMode.Day;
	public LightingObjects lightingObjects;
	public CycleSettings cycleSettings;
	public CycleSettings.Day daySettings;
	public CycleSettings.Night nightSettings;

	//For Testing
	//private uint timeIntervalCounter = 0;


	private Rigidbody sunPhysics;



	// Use this for initialization
	void Start () {
		//Get RigidBody
		this.sunPhysics = this.lightingObjects.sun.gameObject.GetComponent<Rigidbody>();
		this.setDefaultValues ();
		this.setDayOrNight (this.dayOrNight);
	}
	private void setDayNightValues() {
		this.daySettings.beginY = 10;
		this.nightSettings.beginY = 15;
	}
	private void setDefaultValues() {
		this.dayOrNight = CycleMode.Day;
	}
	// Update is called once per frame
	void Update () {
		//this.timeIntervalCounter += 1;
		//if (this.timeIntervalCounter == 15) {
		if (this.dayNightCycleEnabled) {
			if (this.isGameActive) {
				this.rotateSun ();
			}
		}

		float sunPos = this.lightingObjects.sun.transform.rotation.y;
		if (sunPos == this.daySettings.beginY) {
			//Night Time
			this.setDayOrNight (CycleMode.Night);
		} else if (sunPos == this.nightSettings.beginY) {
			this.setDayOrNight(CycleMode.Night);
		}
		//	this.timeIntervalCounter = 0;
		//}

		//When sun rotation equals a certain amount, begin night time transistion.
		//This transition will just turn on night lights
		//When sun reaches next rotation milestone, night time transition is completed, sun object changed to moon color, illumination and all that lowered,
		//Rotation set back to the start point, rotation 

	
	}

	//private void 

	private void rotateSun () {
		switch (System.Convert.ToBoolean((int)this.dayOrNight)) {
		case true:
			//Rotate via day settings
			break;
		case false:
			//Rotate via night settings
			break;
		}
		Quaternion update = Quaternion.Euler (new Vector3(0,this.cycleSettings.sunYRotation,0)  * Time.deltaTime);
		this.sunPhysics.MoveRotation (this.sunPhysics.rotation * update);
		//this.physics.MoveRotation (Quaternion.Euler (1, 1, 0));



		//Transform sunRotation = this.lightingObjects.sun.transform;
		/*this.lightingObjects.sun.gameObject.transform.rotation = */
		//	this.lightingObjects.sun.transform.Rotate (new Vector3 (sunRotation.rotation.x + 0.01f, sunRotation.rotation.y, sunRotation.rotation.z));
	}


	private void cycleDayNight(float cycleLength) {
		//Check current CycleMode,
		//Change to the opposite of current mode boolean, cast to CycleMode type, then determine if it's now day or night, pull length statistics from cycleSettings
	}

	public void setDayOrNight(CycleMode mode) {
		this.turnAllLightsOn (System.Convert.ToBoolean((int)mode));
	}

	private void turnAllLightsOn (bool turnOn) {
		foreach (Light light in this.lightingObjects.lights) {
			light.gameObject.SetActive (turnOn);
		}
	}

	public void setGameActive(bool active) {
		this.isGameActive = active;
	}
}
