﻿//Eric Tomasso
//11/16/2016

using UnityEngine;
using System.Collections;

//GameController handles
//•Game Settings
//•Communication between active game components (EventController, PickupController, TrafficController, PlayerController, StatController)
//•Links the movement controls of PlayerController to the movement of menu items in UIController when appropriate

//To Do or whatever: - means needs to be done, + means done
//+ScoreMultiplier Communications
//-PickupController
//-EventController
//-StatController
//-UIController
//-TrafficController

[System.Serializable]
public class GameSettings
{
	//Define a time delta between score increase - Revised: Just use the DeltaTime function for this
	public uint baseScoreValue;
	//The value a single score point is actually worth. The score will increase by this point value at minimum.
	//Through calculations I believe the best number would be an even number below 10, because the way I
	//Have written the score stuff, int is multiplied by float, but then stored again in an int, so anything after the decimal
	//goes away. An even number multiplied by 1/2 is always a full number.
}

public class GameController : MonoBehaviour
{
	public GameSettings settings;
	
	public AudioController audioController;
	public PickupController pickupController;
	public EventController eventController;
	public TrafficController trafficController;
	public StatController statController;
	public UIController uiController;
	public PlayerController playerController;
	public ScoreController scoreController;
	public DayNightCycleController dayNightCycleController;

	[System.Serializable]
	public class Keybindings
	{
		public KeyCode menuKey = KeyCode.Escape;
		public KeyCode endGameKey = KeyCode.F4;
		public KeyCode moveUp = KeyCode.W;
		public KeyCode moveDown = KeyCode.S;
		public KeyCode moveLeft = KeyCode.A;
		public KeyCode moveRight = KeyCode.D;
	}

	public Keybindings keyBindings;

	private struct GameState {
		public bool isPlayerAlive;
		public bool isGameActive;
		public bool isGamePaused;
		public DayNightCycleController.CycleMode dayOrNight;


	}
	private GameState gameState;
		


	//private bool isPlayerAlive;
	//Determines whether the player is alive or not
	//Determines if game is paused or not. Soft version of isGameActive, does not reset game, only pauses all functions
	//private bool isGamePaused = false;
	// Use this for initialization
	void Start ()
	{
		//Loads StatController object with data
		this.statController.loadFromDisk (); //This must be done first or Long Term Statistics will be overwritten

		this.startGame ();
	
	}

	private void defaultGameState() {
		this.gameState.dayOrNight = DayNightCycleController.CycleMode.Day;
		this.gameState.isGameActive = true;
		this.gameState.isGamePaused = false;
		this.gameState.isPlayerAlive = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (this.keyBindings.menuKey)) {
			Debug.Log ("Menu Key Pressed");
			this.toggleGamePaused ();

		}
			
	}

	public void startGame ()
	{
		if (!this.gameState.isGameActive) {
			this.resetAllComponents ();
			this.gameState.isGameActive = true;
			this.gameState.isGamePaused = false;
			//Tell ScoreController to start counting score
			this.scoreController.enableScoreCounting (this.gameState.isGameActive);
			//Tell TrafficController to start spawning traffic
			this.trafficController.enableTrafficSpawning (this.gameState.isGameActive);
			//Tell PickupController to start spawning pickups
			this.pickupController.enablePickupSpawning (this.gameState.isGameActive);
			//Tell StatController to start saving data
			this.statController.setGameActive (this.gameState.isGameActive, true);
			//Tell StatController to increase Total Games Played
			this.statController.increaseTotalGameCount ();
			//Tell AudioController game is active
			this.audioController.setGameActive (this.gameState.isGameActive);
			//Activate DayNightCycle
			this.dayNightCycleController.setGameActive (this.gameState.isGameActive);

		}
	}
		
	//Sets game paused or not based on the status of isGamePaused. Takes no arguments.
	//Is called by the action of the ESC key, which will pause the game.
	private void toggleGamePaused ()
	{
		Debug.Log ("TOGGLED TO: " + (!this.gameState.isGamePaused).ToString ());
		this.gameState.isGamePaused = !this.gameState.isGamePaused;
		switch (this.gameState.isGamePaused) {
		case false:
			Time.timeScale = 1;
			this.scoreController.enableScoreCounting (true);
			this.trafficController.enableTrafficSpawning (true);
			this.pickupController.enablePickupSpawning (true);
			this.uiController.setGamePaused (false);
			this.audioController.setGamePaused (false);
			break;
		case true:
			Time.timeScale = 0;
			this.scoreController.enableScoreCounting (false);
			this.trafficController.enableTrafficSpawning (false);
			this.pickupController.enablePickupSpawning (false);
			this.uiController.setGamePaused (true);
			this.audioController.setGamePaused (true);
			//this.isGamePaused = true;
			break;
		default:
			break;
			
		}
	}

	//Not sure how this is different from endGame.
	//Wait NVM gameOver will display a game over message while calling endGame, while endGame is neutral and just ends the game
	public void gameOver ()
	{

	}

	public void endGame ()
	{
		if (this.gameState.isGameActive) {
			//Tell ScoreController to stop counting score
			this.scoreController.enableScoreCounting (false);
			//Tell TrafficController to stop spawning traffic
			this.trafficController.enableTrafficSpawning (false);
			//Tell PickupController to stop spawning pickups
			this.pickupController.enablePickupSpawning (false);
			//Tell StatController game has ended
			this.statController.setGameActive (false, true);
			this.gameState.isGameActive = false;
			this.gameState.isGamePaused = false;
		}

	}

	//May not be needed, see notes
	public void restartGame ()
	{
		//I think this and startGame() are the same.
		//All Games will end with endGame() and start with startGame();
		
	}

	//Needed before restartGame() and startGame() to prime field for new game
	private void resetAllComponents ()
	{
		//Reset score to 0 
		//this.statController.resetScore(); THIS IS DONE EVERY TIME A GAME IS ENDED via the StatController.setGameActive(false) method. 
		//Reset score multiplier to 0
		//Reset all PickupLevels to 0
		//Reset all pickups
		this.endAllPickups ();
	}



	#region Settings

	public uint getBaseScoreValue ()
	{
		return this.settings.baseScoreValue;
	}

	#endregion

	#region From PickupController

	//A pickupLevel of ZERO deactivates a pickup, and is the only way PickupController can tell GameController to deactivate one
	//Any changes to pickups will be sent to StatController from this command.
	public void activatePickup (PickupController.PickupType pickup, uint pickupLevel)
	{ 
		
		//check if level is 0, if it is, deactivate pickup
		if (pickupLevel == 0)
			this.endPickup (pickup);

		//It even lets statController know the pickup has ended too, so the UI will be updated automatically from here
		//This will still be used even when the pickupLevel is 0, since a pickupLevel of 0 sent to StatCollector is 
		//ignored in stat totals but will cause the UI to update

		switch (pickup) {
		case PickupController.PickupType.CarDestroyer: 
			//Needs Implementation, but only if pickupLevel > 0
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.CarDestroyer, pickupLevel);
			break;
		case PickupController.PickupType.DecreasePlayerSpeed: //To PlayerController
			if (pickupLevel > 0)
				this.playerController.decreasePlayerSpeed ();
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.DecreasePlayerSpeed, pickupLevel);
			break;
		case PickupController.PickupType.DecreaseTrafficSpeed: //To TrafficController
			if (pickupLevel > 0)
				this.trafficController.decreaseTrafficSpeed (pickupLevel);
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.DecreaseTrafficSpeed, pickupLevel);
			break;
		case PickupController.PickupType.IncreasePlayerSpeed: //To PlayerController
			if (pickupLevel > 0)
				this.playerController.increasePlayerSpeed ();
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.IncreasePlayerSpeed, pickupLevel);
			break;
		case PickupController.PickupType.IncreaseTrafficSpeed: //To TrafficController
			if (pickupLevel > 0)
				this.trafficController.increaseTrafficSpeed (pickupLevel);
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.IncreaseTrafficSpeed, pickupLevel);
			break;
		case PickupController.PickupType.ScoreMultiplier: //To ScoreController
			this.scoreController.setMultiplier (pickupLevel); 
			this.tellStatControllerOfPickupChanges (PickupController.PickupType.ScoreMultiplier, pickupLevel);
			break;
		default:
			break;
		}
	}
	//Main usage would probably be to restart the game
	private void endAllPickups ()
	{
		//I feel like this should do its own thing since this is just PickupController telling GameController to tell PickupController
		//to call GameController 6 times.
		//HOWEVER, this function is only here for when games are ending so perhaps I should call this one "EndGame" or something.
		//We can figure this out later.
		//If this is endGame then it needs to tell StatController to save its stats and reset scores and stuff.
		//But there's already a function that does this.
		//This would be more like QUIT game.

		this.pickupController.resetAllPickupLevels ();
	}

	//Let StatController know about it
	//Private, called when activatePickup receives a level of 0
	//ONLY undoes effects
	private void endPickup (PickupController.PickupType pickup)
	{
		switch (pickup) {
		case PickupController.PickupType.CarDestroyer:
			break;
		case PickupController.PickupType.DecreasePlayerSpeed:
			this.playerController.resetPlayerSpeed ();
			break;
		case PickupController.PickupType.DecreaseTrafficSpeed:
			//this.trafficController.resetTrafficSpeed ();
			break;
		case PickupController.PickupType.IncreasePlayerSpeed:
			goto case PickupController.PickupType.DecreasePlayerSpeed;
		case PickupController.PickupType.IncreaseTrafficSpeed:
			goto case PickupController.PickupType.DecreaseTrafficSpeed;
		//case PickupController.PickupType.ScoreMultiplier:
		//	this.scoreController.setMultiplier (0);
		//	break;
		//^^^This is omitted because activatePickup(0) works on its own on ScoreMultiplier
		default:
			break;
		}
	}

	#endregion

	//Tells StatController about it
	public void addScorePoints (uint points)
	{
		this.statController.increaseScoreCount (points);
	}

	//Tell StatController to increase car spawn total
	public void increaseTotalCarSpawnCount ()
	{
		this.statController.increaseTotalCarsSpawnedCount ();
	}


	public void increaseTotalGameTimeByOneMinute ()
	{
		this.statController.increaseGameTime ();
	}

	//For Updating UI and recording game time
	public void reportGameTime (uint minutes, uint seconds, uint mSeconds)
	{
		this.statController.updateGameTime (minutes, seconds, mSeconds);


	}

	#region To StatController

	private void tellStatControllerOfPickupChanges (PickupController.PickupType pickup, uint level)
	{
		this.statController.changeCurrentPickupLevel (pickup, level);
	}
	//EventController

	#endregion


	#region For StatController

	//Tell StatController when to start taking point increases
	//Tell StatController when to stop increasing points because player has died
	//Score Multiplier changes are currently handled only by

	#endregion

	#region Input for Menus


	#endregion

	#region From DayNightCycleController

	#endregion
}
