﻿//Eric Tomasso
//12/6/16

//Maybe use proper setter / getter later
using UnityEngine;
using System.Collections;

public class PickupTracker {
	public uint _up = 0;
	public uint _down = 0;

	public void increaseUp() {
		this._up += 1;
		this._down = 0;
	}

	public void increaseDown() {
		this._up = 0;
		this._down += 1;
	}

	public void setUp(uint value) {
		this._up = value;
		this._down = 0;
	}

	public uint getUp() {
		return this._up;
	}

	public void setDown(uint value) {
		this._down = value;
		this._up = 0;
	}

	public uint getDown() {
		return this._down;
	}


}
