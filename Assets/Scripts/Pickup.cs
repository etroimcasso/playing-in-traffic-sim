﻿//Eric Tomasso
//11/16/2016


//ALL THIS NEEDS NOW IS TO ROTATE OR SOMETHING

using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour
{
	private PickupController pickupController;


	[System.Serializable]
	public class PersistanceTimeRange
	{
		public float minTime = 5;
		public float maxTime = 10;
	}

	public PersistanceTimeRange persistTime; //Make this a range


	//Parent of object
	public PickupController.PickupType pickupType;
	//private Rigidbody physics;
	//private MeshRenderer meshRender;
	//private Material material;
	//private ulong pickupUID;

	private float existenceTime = 0;
	private float totalExistenceTime;

	// Use this for initialization
	void Start ()
	{
		//this.physics = GetComponent<Rigidbody> ();
		this.pickupController = this.gameObject.GetComponentInParent<PickupController> ();
		//this.collider = this.gameObject.GetComponent<BoxCollider> ();
		//this.material = GetComponent<Material> ();
		//this.meshRender = GetComponent<MeshRenderer> ();

		//Change this destroySelf thing to instead count upwards towards the number of seconds
		//to persist, with Invoke being 1 second at a time.
		//That way, I can have the object slowly fade away as
		//Its current time approaches its total allowed time,
		//Allowing me to use some math to have it slowly fade away as it becomes transparent 
		//then disappears
		this.totalExistenceTime = this.getRandomPersistTime();
		//this.pickupUID = this.generateUniqueId ();

		InvokeRepeating ("countDownToDestruction", 0.1f, 0.1f);

	}

	private ulong generateUniqueId() {
		return System.Convert.ToUInt64(System.Math.Abs (System.DateTime.Now.ToBinary ()));
	}

	private float getRandomPersistTime() {
		return Random.Range(persistTime.minTime, persistTime.maxTime);
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	//EPIC NAME
	//But really it's just a timer :(
	private void countDownToDestruction() {
		//100 - (( 100 / totalExistenceTime) * existenceTime)
		//Color tempColor = this.meshRender.material.color;
		//Debug.Log ("The Thing: " + (100 - ((100 / this.totalExistenceTime) * existenceTime)));
		//tempColor.a = ((100 / this.totalExistenceTime) * existenceTime);
		//this.meshRender.material.color = tempColor;

		if (System.Convert.ToUInt32(this.existenceTime) == System.Convert.ToUInt32(this.totalExistenceTime)) {
			this.destroySelf ();
		}
		this.existenceTime += 0.1f;
	}
	//Alert PickupController of pickup being touched
	void OnTriggerEnter (Collider other)
	{
		//If collide with player, activate pickup.
		//if collide with other pickup, deactivate the newly spawned pickup
		switch (other.tag) {
		case "Player":
			this.pickupController.activatePickup (this.pickupType);
			Destroy (other.gameObject);
			break;
		case "Pickup":
			Destroy (other.gameObject);
			break;
		case "Vehicle":
			//Physics.IgnoreCollision (this.collider, other);
			Destroy(this.gameObject);
			break;
		default:
			break;
		}
	}
	private void destroySelf() {
		Destroy (this.gameObject);
	}
}
