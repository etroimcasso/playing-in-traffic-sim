﻿//Eric Tomasso
//12-7-16
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//The goal is to have a jukebox item that contains references to all the different audio files
//And can produce a random sound clip from a SoundType
[System.Serializable]
public class JukeBox {
	public enum SoundType {
		Music = 0,
		SFX = 1
	}

	public enum SFXType {
		Traffic = 0,
		Ambient = 1,
		Player = 2,
		Vehicle = 3

	}
	public List<AudioClip> sfxList;

	public List<AudioClip> musicList;
	

	// Use this for initialization
	void Start () {
		this.sfxList = new List<AudioClip> ();
		this.musicList = new List<AudioClip> ();
	
	}



	public AudioClip getRandomAudio(SoundType type) {
		switch (type) {
		case SoundType.Music:
			return this.musicList [Random.Range (1, this.musicList.Count)];
		case SoundType.SFX:
			return this.sfxList [Random.Range (1, this.sfxList.Count)];
		default:
			return null;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
