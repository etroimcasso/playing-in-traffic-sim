﻿//Eric Tomasso
//11/16/2016

using UnityEngine;
using System.Collections;



//PlayerController's Controller Input should control either a UI menu selector or 
//the player object depending on isMenuActive.

//This could probably control UIController without GameController being needed.
//GameController is required for determining if game is active or paused. This behavior may change.

//Player speed is determined by base speed and a speed multiplier.


public class PlayerController : MonoBehaviour {
	public GameController gameController;
	public UIController uiController; //Needs UIController for controlling UIs with the input keys


	public float baseSpeed = 1.0f; //Original Speed
	public float playerSpeed; // = base speed + (speedincrement * pickuplevel). However, the way this works, pickuplevel isn't needed in this class.
	public float speedIncrement = 1.0f;


	private bool isMenuActive; //Changed by the menu button

	// Use this for initialization
	void Start () {
		this.resetPlayerSpeed ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	//I'll probably use the same formula for this as the last one, maybe tweak it a little so it has less affect.
	//Regardless, all base multiplier levels are 1;
	#region From GameController
	//Either of these with a level of 0 will undo their effects
	public void increasePlayerSpeed() {
		this.playerSpeed += this.speedIncrement;
	}

	public void decreasePlayerSpeed() {
		this.playerSpeed -= this.speedIncrement;
	}
	#endregion

	public void resetPlayerSpeed() {
		this.playerSpeed = this.baseSpeed;
	}

	
}

