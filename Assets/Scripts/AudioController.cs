﻿//Eric Tomasso
//12-7-16

using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

//Controls all Audio for other Controllers.
//CAN BE ACCESSED WITHOUT GAMECONTROLLER FOR SPEED PURPOSES


//Needs to play Game Music when isGameActive = true;


public class AudioController : MonoBehaviour {
	//Jukebox contains a catalog of all audio in use.
	//Also provides randomization of audio clips and the like
	public JukeBox soundClips = new JukeBox();
	public AudioSource musicSource;
	public AudioSource sfxSource = null;

	private bool isGameActive = false;
	private bool isGamePaused = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		this.decideIfMusicPlays ();

	}

	public void setGameActive (bool active) {
		this.isGameActive = active;

	}

	public void setGamePaused (bool paused) {
		this.isGamePaused = paused;
	}

	private void playAudio(AudioClip audio) {

	}

	//Determines if music should be playing or paused, depending on the status of isGameActive.
	//True: Play/unpause music if not playing
	//False: Pause music is music is playing
	private void decideIfMusicPlays() {
		if (this.isGameActive) {
			switch (this.isGamePaused) {
			case false:
				if (this.musicSource.isPlaying == false)
					this.musicSource.Play ();
				break;
			case true:
				if (this.musicSource.isPlaying == true)
					this.musicSource.Pause ();
				break;
			}
		}
	}
}
