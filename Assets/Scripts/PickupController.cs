﻿//Eric Tomasso
//12/11/2016

using UnityEngine;
using System.Collections;

//When a pickup is collected, it uses activatePickup() function

[System.Serializable]
public class PickupSpawnBoundary
{
	public float xMin, xMax, zMin, zMax;
	public float yOffset = 0.47f;
	public GameObject ground;
}


//WHEN SPAWNING, NEEDS TO CHECK IF SPAWN LOCATION IS THE TOO CLOSE TO THE PLAYER OR ANOTHER PICKUP


//needs a pickup timer that activates for certain pickups
//I suggest score multiplier be one that does not reset
//needs a data object that holds a value for value in PickupType enum

public class PickupController : MonoBehaviour
{
	//TESTING
	//private float testFrameLimit = 0;


	public GameController gameController;
	public GameObject pickupSpawnList;
	//public StatController statController; //For handling Pickup Information. PickupController contains information on what is active and what isn;t
	//in the pickupLevels[] array, with 0 meaning not active.

	public float spawnInterval;

	[System.Serializable]
	public class PickupProbabilities
	{
		//This works by having an array of PickupType with a defined size, with the idea being that the size is 100 / probArrayBlockValue.
		//So each spot in the array is a percentage value equal to probArrayBlockValue.
		//This way we can guarantee that 100% probability is properly distributed amongst the 6 pickup types.
		//When calculating the actual probability, the counted number of assigned array blocks for the PickupType in question
		//  is multiplied by the probArrayBlockValue
		public float pickupSpawnProbability = 0.2f;
		public PickupController.PickupType[] probabilityArray = new PickupController.PickupType[25];
		public uint probArrayBlockValue = 4;


		public uint getPickupProbability (PickupController.PickupType pickup)
		{
			uint probability = 0;
			foreach (PickupController.PickupType probCount in this.probabilityArray) {
				if ((int)probCount == (int)pickup) {
					probability += probArrayBlockValue;
				}
			}
			return probability;

		}



	}

	public PickupProbabilities pickupProbabilities;

	[System.Serializable]
	public class PickupObjects
	{
		public GameObject decreaseTrafficSpeed;
		public GameObject increaseTrafficSpeed;
		public GameObject increasePlayerSpeed;
		public GameObject decreasePlayerSpeed;
		public GameObject scoreMultiplier;
		public GameObject carDestroyer;

	}

	public PickupObjects pickupObjects;


	public enum PickupType
	{
		DecreaseTrafficSpeed = 0,
		IncreaseTrafficSpeed = 1,
		IncreasePlayerSpeed = 2,
		DecreasePlayerSpeed = 3,
		ScoreMultiplier = 4,
		CarDestroyer = 5,
		//We can add more later on easily, the way this controller is set up we just have to add a new name and number,
		//then add it to the activatePickup() function,
		//and add what to do with its effects to the GameController's "activatePickup" function
		//also add portions for PickupStats to StatController and XMLSaveParser's save and load functions
	};


	//Array containing the current levels, the array index corresponds to the value of each enum type
	//Whenever accessing this, don't use the numbers specifically, instead use [(int)PickupType] and the PickupType enum
	//to access the value you want
	//That way we can move the numbers around if we ever feel like and it will still work
	private uint[] pickupLevels = new uint[6];
	//uint because levels never go below 0


	//Pickups should not spawn outside of this boundary.
	//Even easier would be to use a Plane object as the boundary, to keep the pickups within the object's mesh coordinates.
	public PickupSpawnBoundary boundary;


	private bool arePickupsSpawning = false;

	public bool ArePickupsSpawning {
		get {
			return arePickupsSpawning;
		}
	}


	// Use this for initialization
	void Start ()
	{
		//this.pickupObject.getRandomPickupType ();
		//Fills pickupLevels array for first use
		for (int i = 0; i <= 5; i++) {
			this.pickupLevels [i] = 0;
		}
		InvokeRepeating ("spawnPickup", this.spawnInterval, this.spawnInterval);
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

			//private void 

	void spawnPickup ()
	{
		//Want X and Z to be random, with Y being the Y position of the GROUND object + an interval that makes it not clip through the ground
		//Generate random X and Z from the SpawnBoundary information
		//Then feed this information to the second parameter in the instantiate method
		//This should allow pickups to spawn around the playfield correctly.
		//Need to make sure Pickups don't overlap as well.
		//Also pickups don't ALWAYS spawn. Make pickup spawning random too.
		if (this.arePickupsSpawning) {
			if (this.shouldSpawnPickup ()) {
				PickupType pickupType = this.getRandomPickupType ();
				Transform parent = this.pickupSpawnList.transform;
				Vector3 spawnLocation = new Vector3 (
					                       Random.Range (this.boundary.xMin, this.boundary.xMax),
					this.boundary.ground.transform.position.y + this.boundary.yOffset,
					                       Random.Range (this.boundary.zMin, this.boundary.zMax)
				                       );
				switch (pickupType) {
				case PickupType.CarDestroyer:
					Instantiate (this.pickupObjects.carDestroyer, spawnLocation, Quaternion.identity, parent);
					break;
				case PickupType.DecreasePlayerSpeed:
					Instantiate (this.pickupObjects.decreasePlayerSpeed, spawnLocation, Quaternion.identity, parent);
					break;
				case PickupType.DecreaseTrafficSpeed:
					Instantiate (this.pickupObjects.decreaseTrafficSpeed, spawnLocation, Quaternion.identity, parent);
					break;
				case PickupType.IncreasePlayerSpeed:
					Instantiate (this.pickupObjects.increasePlayerSpeed, spawnLocation, Quaternion.identity, parent);
					break;
				case PickupType.IncreaseTrafficSpeed:
					Instantiate (this.pickupObjects.increaseTrafficSpeed, spawnLocation, Quaternion.identity, parent);
					break;
				case PickupType.ScoreMultiplier:
					Instantiate (this.pickupObjects.scoreMultiplier, spawnLocation, Quaternion.identity, parent);
					break;
				}
				//For testing only
				//Debug.Log("PICKUP: " + pickupType.ToString());
				this.activatePickup (pickupType);
			}
		}
	}

	private bool shouldSpawnPickup ()
	{
		return (Random.Range (0, 100) <= this.pickupProbabilities.pickupSpawnProbability * 100 );// ? true: false;
	}

	private void increasePickupLevel (PickupType pickup)
	{
		this.pickupLevels [(int)pickup] += 1;
		//Let GameController know about the change
		//this.gameController.activatePickup (pickup, this.pickupLevels [(int)pickup]);
	}

	private void resetPickupLevel (PickupType pickup)
	{
		this.pickupLevels [(int)pickup] = 0;
		this.gameController.activatePickup (pickup, this.pickupLevels [(int)pickup]);
	}

	public void resetAllPickupLevels ()
	{ //Is public only so GameController can reset pickup levels for a new game
		//Start at 0, iterate to highest number in enum
		//Do we iterate through the enum so we can expand the number of pickups later, or hard-code the stuff here?
		//Nah just do it explicitly for each one for now

		this.pickupLevels [(int)PickupType.CarDestroyer] = 0;
		this.gameController.activatePickup (PickupType.CarDestroyer, 0); //have to let the GameController know about it too

		this.pickupLevels [(int)PickupType.DecreasePlayerSpeed] = 0;
		this.gameController.activatePickup (PickupType.DecreasePlayerSpeed, 0);

		this.pickupLevels [(int)PickupType.DecreaseTrafficSpeed] = 0;
		this.gameController.activatePickup (PickupType.DecreaseTrafficSpeed, 0);

		this.pickupLevels [(int)PickupType.IncreasePlayerSpeed] = 0;
		this.gameController.activatePickup (PickupType.IncreasePlayerSpeed, 0);

		this.pickupLevels [(int)PickupType.IncreaseTrafficSpeed] = 0;
		this.gameController.activatePickup (PickupType.IncreaseTrafficSpeed, 0);

		this.pickupLevels [(int)PickupType.ScoreMultiplier] = 0;
		this.gameController.activatePickup (PickupType.ScoreMultiplier, 0);
	}


	//called by children processes ONLY unless otherwise needed
	public void activatePickup (PickupType pickup)
	{
		//Increment pickup level by 1
		//Opposing pickup types (like Traffic Up/Down) need to reset the opposite count to 0 when picked up
		//goto default; line is required for pickup level to increment
		//ScoreMultiplier and CarDestroyer are not in this list because they do not have opposing types (at least for now)
		///Their activation falls directly into the default case
		/// endPickup by using a level of 0 when using GameController.activatePickup
		switch (pickup) {
		case PickupType.IncreasePlayerSpeed:
			this.resetPickupLevel (PickupType.DecreasePlayerSpeed); //Resets DecreasePlayerSpeed to 0
			this.gameController.activatePickup (PickupType.DecreasePlayerSpeed, 0);
			goto default;
		case PickupType.DecreasePlayerSpeed:
			this.resetPickupLevel (PickupType.IncreasePlayerSpeed);
			this.gameController.activatePickup (PickupType.IncreasePlayerSpeed, 0);
			goto default;
		case PickupType.IncreaseTrafficSpeed:
			this.resetPickupLevel (PickupType.DecreaseTrafficSpeed);
			this.gameController.activatePickup (PickupType.DecreaseTrafficSpeed, 0);
			goto default;
		case PickupType.DecreaseTrafficSpeed:
			this.resetPickupLevel (PickupType.IncreaseTrafficSpeed);
			this.gameController.activatePickup (PickupType.IncreaseTrafficSpeed, 0);
			goto default;
		default:
			this.increasePickupLevel (pickup);
			//Debug.Log ("Pickup Spawned: " + pickup.ToString ());
			break;
		}

		//Applies Pickup via GameController
		gameController.activatePickup (pickup, pickupLevels [(int)pickup]);
	}


	public void enablePickupSpawning (bool isSpawning)
	{
		this.arePickupsSpawning = isSpawning;
	}

	private PickupController.PickupType getRandomPickupType ()
	{
		uint items = (uint)PickupType.GetNames (typeof(PickupType)).Length;
		//Needs to generate based on probabilities from PickupController.PickupProbabilities
		//To generate probability:
		//Add all probability numbers together
		//Divide that by 6
		//Multiply each number individually by the /6 number to get the number each ratio point is worth 

		//Take a number from the getPickupProbability() thing from PickupController.PickupProbabilities
		uint[] probabilities = new uint[items];
		uint[] probMin = new uint[items];
		uint[] probMax = new uint[items];

		//Fill probabilities array
		for (int i = 0; i < (int)items; i++) {
			probabilities [i] = this.pickupProbabilities.getPickupProbability ((PickupController.PickupType)i);
		}


		//Now that I have the total probabilities / 100 for each pickup type,
		//max starts out as 0
		//min = max + 1
		//max = (min - 1) + prob
		uint max = 0;
		for (int i = 0; i < (int)items; i++) {
			probMin [i] = max + 1;
			probMax [i] = (probMin [i] - 1) + probabilities [i];
			max = probMax [i];

		}

		//Now generate a random number, and from that equal the #i to the int of the PickupType enum
		//For each range in the i thing.
		//Whatever it'll make sense when I build it
		int randomNumber = Random.Range (1, 100);
		for (int i = 0; i < (int)items; i++) {
			if (randomNumber >= probMin [i] && randomNumber <= probMax [i]) {
				return (PickupController.PickupType)i;
			}
		}
		//If it gets here, it dun goofed
		Debug.LogError ("PICKUP ERROR");
		return (PickupController.PickupType)(Random.Range (0, (int)items - 1));
	}
}
