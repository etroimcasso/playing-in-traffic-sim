﻿//Eric Tomasso
//11/16/2016

//THIS FILE IS COMPLETE!
/// UNLESS IT NEEDS TO COUNT HOURS TOO
using UnityEngine;
using System.Collections;

//Despite its name, ScoreController only handles the generation of the score value to add during each score interval.
//StatController handles the actual storage of the current score.
///StatController.increaseScoreScount(uint newScore) tells StatController what to add to the score
///ScoreController calls this function ever DeltaTime as long as isScoreCounting is True.
/// isScoreCounting can be set true/false via GameController using ScoreController.countScore(boolean)
///GameController changes the ScoreController's multiplier level via the Pickups functions
/// 

public class ScoreController : MonoBehaviour {
	public GameController gameController; //GameController tells ScoreController when to start and stop adding to the score
											//Also tells ScoreController when to change the multiplier level.
												//so GameController also needs to tell StatController about the changes
	//public StatController statController;

	private bool isScoreCounting = false;
	public bool IsScoreCounting {
		get {
			return isScoreCounting;
		}
	}

	//This is used for tracking minutes passed
	private uint secondsCounter = 0;
	private uint minutesCounter = 0;
	private uint mSecondsCounter = 0;

	private uint baseScoreValue;
	private uint scoreMultiplierLevel;
	public float scoreMultiplier;

	// Use this for initialization
	void Start () {
		this.baseScoreValue = this.gameController.settings.baseScoreValue;
		this.scoreMultiplierLevel = 0;
		this.setMultiplier (this.scoreMultiplierLevel);

		//Score Counter
		InvokeRepeating ("incrementScore", 0.2f, 0.2f);

		//Time Counter
		InvokeRepeating ("countTime", 0.1f, 0.1f);
	
	}
	
	// Update is called once per frame
	void Update () {
		//Every time delta, increase the score by baseScoreValue 
		/*
		if (this.isScoreCounting) {
			//Tell StatCounter about new score
			//This should go through GameController, via something like GameController.addToScore(uint pointsToAdd)
			//Only StatController can talk to another object without GameController's participation
			this.gameController.addScorePoints(this.calculateScore());
		}
		*/
	
	}

	public void resetCounts() {
		this.scoreMultiplierLevel = 0;
		this.setMultiplier (this.scoreMultiplierLevel);

	}

	private void countTime() {
		if (this.isScoreCounting) {
			if (this.mSecondsCounter < 9) {
				this.mSecondsCounter += 1;
			} else if (this.mSecondsCounter == 9) {
				this.mSecondsCounter = 0;
				if (this.secondsCounter != 59) {
					this.secondsCounter += 1;
				} else if (this.secondsCounter == 59) {
					this.secondsCounter = 0;
					this.minutesCounter += 1;
					this.gameController.increaseTotalGameTimeByOneMinute ();
				}
			}
			this.gameController.reportGameTime (minutesCounter, secondsCounter, mSecondsCounter);
		}
	}

	//Increments score by calculated amount
	//But only if isScoreCounting = true
	private void incrementScore() {
		if (this.isScoreCounting) {
			this.gameController.addScorePoints(this.calculateScore());
		}
	}


		
	public void enableScoreCounting(bool shouldCountScore) {
		this.isScoreCounting = shouldCountScore;
	}

	uint calculateScore() { //returns the baseScoreValue multiplied by the scoreMultiplier
		//Calculate amount to add to score
		//(X+1) - (X / 2)
		float newScore = this.baseScoreValue  * this.scoreMultiplier;
		return (uint)newScore ; //Partial Points not counted
	}
		

	public void setMultiplier (uint multiplierLevel)
	{ //Uses the formula (X+1) - (X/2) to calculate score multiplier. Even works with 0 and negative numbers
		//Score multiplier will increment by .5, with the default level being 0 and a 1.0 score multiplier
		//Sets the scoreMultiplierLevel and calculates the new value for the Score Multiplier
		this.scoreMultiplierLevel = multiplierLevel;
		this.scoreMultiplier = ((multiplierLevel + 1) - (multiplierLevel / 2));
		//StatMultiplier is already alerted of changes through GameController's reporting of Pickup changes
	}

	public uint getMultiplierLevel () {
		return this.scoreMultiplierLevel;
	}


}
