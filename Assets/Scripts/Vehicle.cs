﻿//Eric Tomasso
//12/11/16
using UnityEngine;
using System.Collections;

//Nestor's code was shit and has been completely removed.

//AI FOR VEHICLES
//OBJECT DESTRUCTION WILL OCCUR ON COLLISION WITH A TAGGED "CAR_DESTROYER" TAG


public class Vehicle : MonoBehaviour
{

	private Vector3 facingDirection = Vector3.right; //Dirty hack thanks to Ryan's 3d models being all fucked

	[System.Serializable]
	public struct VehicleSettings
	{
		public TrafficController.VehicleType vehicleType;
		public float baseStartSpeed;
		public float minimumSpeed;
		public float maximumSpeed;
		public float maxStartSpeedVariation;
		//The range for variations in the spawn speed for each vehicle instance. Allows for more variation.
		public float viewDistance;
		//Distance the AI can see to react to other vehicles / the player / etc.
		public float brakingDistance;
		//Distance this vehicle will attempt to brake. LATER MAKE THIS A RANDOM RANGE AFTER IT WORKS
		public int[] allowedLanes;
	}

	public VehicleSettings typeSettings;

	private enum RandomSpeedTypes
	{
		FullRange,
		MinimumPlusRange,
		MaximumMinusRange,
		StartSpeed
	};



	//public float brakeTimer;    //Time it takes for the player to brake
	private TrafficController trafficController;

	private Rigidbody physics;
	private Collider collisions;


	public float spawnedSpeed;
	public float currentSpeed;
	public float minSpeed;
	public float maxSpeed;
	//public float[] speedRange;


	private float velocityMultiplier = 10f;

	// int DecisionTime;


	/*####################################################################################
    ##################################################################################### */
	// Use this for initialization
	// Use this for initialization
	void Start ()
	{
		this.physics = this.gameObject.GetComponent<Rigidbody> ();
		this.collisions = this.gameObject.GetComponent<BoxCollider> ();
		this.trafficController = GameObject.Find ("TrafficController").GetComponent<TrafficController> ();
		//Debug.Log ("Traffic COntroller found? " + (this.trafficController == null ? "No" : "yes") + " But this piece of shit is really: " + this.trafficController.name);
		//this.instanceParams = new VehicleInstanceParameters ();

		this.initializeVehicle ();

		//Debug.Log ("Spawned Speed: " + this.instanceParams.spawnedSpeed);

		this.setVehicleInMotion ();

		//this.speed = Random.Range(this.minSpeed, this.maxSpeed);     //generate a random car speed within this range
		//this.originalSpeed = this.speed;
		//this.brakeTimer = Random.Range(5f, 10f);//timer for the when the car stops

		//Get TrafficController
		//this.trafficController = this.GetComponentInParent<TrafficController> ();

		//   DecisionTime = Random.Range(0, 1);
	}


	private void initializeVehicle ()
	{
		//Set speed, brake distance, view distances
		this.spawnedSpeed = this.getRandomSpeed (RandomSpeedTypes.StartSpeed);
		this.minSpeed = this.getRandomSpeed (RandomSpeedTypes.MinimumPlusRange); //Get Minimum speed w/ variation
		this.maxSpeed = this.getRandomSpeed (RandomSpeedTypes.MaximumMinusRange); // Get Maximum Speed w/ variation


	}

	private void setVehicleInMotion ()
	{
		//this.physics.
		//this.physics.AddForce (Vector3.right * (this.instanceParams.spawnedSpeed * 500f));
		this.currentSpeed = this.spawnedSpeed;
		this.updateSpeed ();
	}

	private float getRandomSpeed (RandomSpeedTypes type)
	{
		float outValue;
		switch (type) {
		case RandomSpeedTypes.FullRange:
			outValue = Random.Range (this.typeSettings.minimumSpeed, this.typeSettings.maximumSpeed);
			break;
		case RandomSpeedTypes.MaximumMinusRange:
			outValue = Random.Range (this.typeSettings.maximumSpeed - this.typeSettings.maxStartSpeedVariation, this.typeSettings.maximumSpeed);
			break;
		case RandomSpeedTypes.MinimumPlusRange:
			outValue = Random.Range (this.typeSettings.minimumSpeed, this.typeSettings.minimumSpeed + this.typeSettings.maxStartSpeedVariation);
			break;
		case RandomSpeedTypes.StartSpeed: //Get start speed by taking RandomRange baseStartSpeed - startSpeedVariation, baseStartSpeed + startSpeedvariation
			outValue = Random.Range (this.typeSettings.baseStartSpeed - this.typeSettings.maxStartSpeedVariation, this.typeSettings.baseStartSpeed + this.typeSettings.maxStartSpeedVariation);
			break;
		default:
			outValue = 1f;
			break;
		}
		return Mathf.Abs (outValue);

	}

	// Update is called once per frame
	void Update ()
	{

	}



	//Destroy Cars if they crash into each other
	public void OnTriggerEnter (Collider other)
	{
		switch (other.tag) {
		case ("Vehicle"):  //look for vehicle 
			
			break;
		case ("Pickup"):
			Physics.IgnoreCollision (this.collisions, other);
			break;
		}

		if (other.name.ToString ().Equals ("Vehicle Despawner")) {
			Destroy (this.gameObject);
		}

	}



	public TrafficController.VehicleType getVehicleType ()
	{
		return this.typeSettings.vehicleType;
	}


	#region Powerup Stuff, from TrafficController, ultimately from

	public void lowerSpeed ()
	{
	//	Debug.Log ("Lower speed for " + this.name);
		if (this.currentSpeed > 0 ) {
			float result = Mathf.Abs( this.currentSpeed - trafficController.getSpeedIncrement () );
			if (result >= this.minSpeed) {
				this.currentSpeed = result;//this.trafficController.getMultiplierValue();
				this.updateSpeed ();
			}
		}
	}

	public void increaseSpeed ()
	{
	//	Debug.Log ("Increase speed for " + this.name);
		if (this.currentSpeed > 0) {
			float result = this.currentSpeed + this.trafficController.getSpeedIncrement ();
			if (result <= this.maxSpeed) {
				this.currentSpeed = result;//this.trafficController.getMultiplierValue();
				this.updateSpeed ();
			}
		}
	}

	//Not needed
	public void resetSpeed ()
	{
		//this.currentSpeed = this.spawnedSpeed;
	}

	private void updateSpeed ()
	{
		if (this.currentSpeed > 0 )
			this.physics.velocity = this.facingDirection * (this.currentSpeed * this.velocityMultiplier);
	}


	#endregion

	public int[] getValidLanes ()
	{
		return this.typeSettings.allowedLanes;
	}
		
}
